### Результат работы опубликован на Github [Doctor Visit](https://leosame.github.io/doctor_visit)

### Работали студенты:

**Мария Удалова (Mariya Udalova)**
**Алексей Кудряшов (SmilingDog)**
**Леонид Семенюк (LeoSam)**

1. **Мария Удалова (Mariya Udalova):**

- классы Form, Visit, Enter;
- стиль header;
- стили кнопок;
- get, put запросы.

2. **Алексей Кудряшов (SmilingDog):**

- класс Card;
- стиль body;
- pull, delete запросы;
- перемещение карточек.

3. **Леонид Семенюк (LeoSam):**

- работа с git;
- Методы классов (рендер, раздиление на методы);
- финальные стили;
- сборка проэкта, тестирование, устранене багов.

email: a.kudr74@gmail.com
password: a.kudr74
